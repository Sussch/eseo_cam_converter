# Lists of ESEO camera error codes.
#
# Indrek Synter 2015, 2016
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

errlist_modules = {
    0x00: 'System',
    0x01: 'HAL',
    0x02: 'CTable',

    0x10: 'Command Sch',
    0x11: 'Date-time Sch',
    0x12: 'Periodic Sch',
    0x13: 'Background',

    0x20: 'File system',
    0x21: 'RAM FS 1',
    0x22: 'RAM FS 2',
    0x23: 'FRAM FS 1',
    0x24: 'FRAM FS 2',
    0x25: 'FRAM FS 3',
    0x26: 'FRAM FS 4',
    0x27: 'SDC FS 1',
    0x28: 'SDC FS 2',

    0x30: 'Error logging',

    0x31: 'Data logging',
    0x32: 'Telemetry',
    0x33: 'Statistics',

    0x40: 'Scripting',

    0x50: 'CAN drivers',
    0x51: 'CAN1',
    0x52: 'CAN2',

    0x60: 'I2C drivers',
    0x61: 'I2C1',
    0x62: 'I2C2',

    0x70: 'SPI drivers',
    0x73: 'SPI3',
    0x74: 'SPI4',
    0x76: 'SPI6',

    0x80: 'FMC',

    0x90: 'Image sensor',
    0x91: 'Current sensor',
    0x92: 'Temp. sensor',
    0x93: 'Camera driver',
    0xA0: 'Image eval',
    0xA1: 'Image compression',

    0xB0: 'SRAM / SDRAM',
    0xB1: 'SDRAM1',
    0xB2: 'SDRAM2',

    0xC0: 'SD drivers',
    0xC1: 'SD1',
    0xC2: 'SD2',
    0xC3: 'SD3',
    0xC4: 'SD4',
    0xCA: 'SD Bank A',
    0xCB: 'SD Bank B',

    0xD0: 'FRAM drivers',
    0xD1: 'FRAM1',
    0xD2: 'FRAM2',

    0xE0: 'AP drivers',
    0xE1: 'AP USB',
    0xE2: 'AP CDC',
    0xE3: 'AP MASS',
    0xE4: 'AP RS485',
    0xF0: 'Bootloader',
    0xF1: 'Firmware'
}

sch_modules = list(range(0x10, 0x12 + 1))
fs_modules = list(range(0x20, 0x28 + 1)) + [0x33]
i2c_modules = list(range(0x60, 0x62 + 1)) + [0x92]
spi_modules = list(range(0x70, 0x74 + 1))
sd_modules = list(range(0xC0, 0xCB + 1))
fram_modules = list(range(0xD0, 0xD2 + 1))
bus_modules = list(range(0x50, 0x52 + 1)) + list(range(0xE0, 0xE4 + 1))
cam_modules = [0x90, 0x93]
proc_modules = [0xA0, 0xA1]
bldr_module = 0xF0

errlist_errors = {
    0x01: 'F: Division by zero',
    0x02: 'F: Unaligned access',
    0x03: 'F: No co-processor',
    0x04: 'F: Invalid PC',
    0x05: 'F: Invalid state',
    0x06: 'F: Undefined instruction',
    0x07: 'F: Stack error',
    0x08: 'F: Unstack error',
    0x09: 'I: Imprecise error',
    0x0A: 'I: Precise error',
    0x0B: 'F: Instruction bus error',
    0x0C: 'F: Memory stack error',
    0x0D: 'F: Memory unstack error',
    0x0E: 'F: Data access violation',
    0x0F: 'F: Instruction access violation',

    0xF0: 'R: Power on',
    0xF1: 'R: Power on / brown-out',
    0xF2: 'R: Pin toggled',
    0xF3: 'R: Low power',
    0xF4: 'R: Independent Watchdog',
    0xF5: 'R: Windowed Watchdog',
    0xF6: 'R: Software reset',
    0xF7: 'R: Boot-up from reset',

    0x10: 'S: Fallback',
    0x11: 'S: Stack overflow',
    0x12: 'S: Malloc failed',
    0x13: 'G: Invalid argument',
    0x14: 'G: Timeout',
    0x15: 'G: Not enough space',
    0x16: 'G: Unimplemented',
    0x17: 'G: Not found',
    0x18: 'G: Read error',
    0x19: 'G: Write error',
    0x1A: 'G: Out of range',
    0x1B: 'G: Busy',
    0x1C: 'G: Not initialized',
    0x1D: 'G: Already initialized',
    0x1E: 'G: Initialization failed',
    0x1F: 'G: Deinitialization failed',

    0x20: 'S:FLASH: Page error',
    0x21: 'S:FLASH: Write protect',
    0x22: 'S:FLASH: Timeout',  # Duplicates G:Timeout
    0x23: 'S:FLASH: Erase',

    0x30: 'G: Assert failed',
    0x31: 'G: General error',  # Something happened .. not sure what
    0x32: 'G: CRC mismatch',
    0xFF: 'G: No recovery'
}

sch_errors = {
    0x01: 'SCH: Missing handler',
    0x02: 'SCH: Invalid handler',
    0x03: 'SCH: Missing arguments',
    0x04: 'SCH: Queue full',
    0x05: 'SCH: Queue corrupt',
    0x06: 'SCH: Queue empty',

    0x09: 'SCH: Read error',  # Duplicates G:Read error
    0x0A: 'SCH: Write error',  # Duplicates G:Write error
    0x0B: 'SCH: Wrong execution mode',

    0x0E: 'SCH: Checksum mismatch',
    0x0F: 'SCH: Block corrupt'
}

i2c_errors = {
    0x01: 'I2C: Device registrar full',
    0x02: 'I2C: Transaction queue full',
    0x03: 'I2C: Invalid device id',
    0x04: 'I2C: Transaction lost',
    0x05: 'I2C: Invalid port',
    0x06: 'I2C: Timer creation failed',
    0x07: 'I2C: Bus busy',
    0x08: 'I2C: Nack',
    0x09: 'I2C: Arbitration lost',
    0x0A: 'I2C: Bus error',
    0x0B: 'I2C: Bus overrun / underrun',
    0x0C: 'I2C: Unknown event',
    0x0D: 'I2C: Sanity failure'
}

spi_errors = {
    0x01: 'SPI: Device registrar full',
    0x02: 'SPI: Transaction queue full',
    0x03: 'SPI: Invalid device id',
    0x04: 'SPI: Transaction lost',
    0x05: 'SPI: Invalid port',
    0x06: 'SPI: Timer creation failed',
    0x07: 'SPI: Bus busy',
    0x08: 'SPI: Tx error',
    0x09: 'SPI: Rx error',
    0x0A: 'SPI: Bus error',

    0x0D: 'SPI: Sanity failure'
}

hal_errors = {
    0x01: 'HAL: No DMA channel'
}

bus_errors = {
    0x01: 'BUS: Rx overflow',
    0x02: 'BUS: Tx overflow',
    0x03: 'BUS: Rx error',
    0x04: 'BUS: Tx error',
    0x05: 'BUS: Rx buffer overflow',
    0x06: 'BUS: Rx invalid packet',
    0x07: 'BUS: CAN error',
    0x08: 'BUS: CAN warning',
    0x09: 'BUS: CAN passive',
    0x0A: 'BUS: IO locked for DMA'
}

sd_errors = {
    0x0E: 'SD: Critical sector broken',
    0x0F: 'SD: Critical sector recovered'
}

fs_errors = {
    0x01: 'FS: Too many mounted',
    0x02: 'FS: Unsupported operation',
    0x03: 'FS: File / directory already exists',
    0x04: 'FS: Invalid file mode',
    0x05: 'FS: Volume full',
    0x06: 'FS: File full',
    0x07: 'FS: Invalid seek address',
    0x08: 'FS: Attempt to format a mounted fs',
    0x09: 'FS: IO locked for DMA',
    0x0A: 'FS: No file system on volume'
}

fram_errors = {
}

cam_errors = {
    0x01: 'DCMI: Overflow',
    0x02: 'DCMI: Sync error',
    0x03: 'DCMI: Timeout',
    0x04: 'DMA: Transfer error',
    0x05: 'DMA: FIFO error',
    0x06: 'DMA: Direct mode error',
    0x07: 'DMA: Timeout',
    0x08: 'Stop failed'
}

proc_errors = {
    0x03: 'RAM: Wakeup failed',
    0x31: 'G: Compression failed',
}

bldr_errors = {
    0x01: 'BL:S: Log initialized',
    0x02: 'BL:S: Slots initialized',
    0x03: 'BL:S: Commandlist initialized',
    0x04: 'BL:S: Command handled',
    0x05: 'BL:S: Commandlist cleared',
    0x06: 'BL:S: Slot verified',
    0x07: 'BL:S: Copying firmware',
    0x08: 'BL:S: Jumping to firmware',
    0x09: 'BL:S: Command list empty',
    0x10: 'BL:S: Firmware copy failed',
    0x11: 'BL:S: BootTo command failed',
    0x12: 'BL:E: Invalid argument',
    0x13: 'BL:E: Timeout',
    0x14: 'BL:E: Read error',
    0x15: 'BL:E: Write error',
    0x16: 'BL:E: Erase error',
    0x17: 'BL:E: CRC mismatch',
    0x18: 'BL:E: Commandlist corrupt',
    0x19: 'BL:E: Invalid command',
    0x1A: 'BL:E: Invalid slot',
    0x1B: 'BL:E: Slot length mismatch',
    0x1C: 'BL:E: Log init failed',
    0x1D: 'BL:E: Log read failed'
}

errtree = [
    # Dictionary, nickname, expanded name
    (errlist_errors, 'generic', 'generic'),
    (hal_errors, 'HAL', 'hardware abstraction layer'),
    (sch_errors, 'sch', 'command scheduler'),
    (fs_errors, 'fs', 'file system'),
    (i2c_errors, 'I2C', 'I2C'),
    (spi_errors, 'SPI', 'SPI'),
    (sd_errors, 'SD', 'SD card'),
    (cam_errors, 'CAM', 'camera'),
    (bus_errors, 'bus', 'bus'),
    (bldr_errors, 'boot', 'bootloader')
]


def format_module(err_module):
    """Formats module code into text form."""
    global errlist_modules
    if err_module in errlist_modules:
        return errlist_modules[err_module]
    return hex(err_module)


def format_error(error, err_module):
    """Formats error code into text form."""
    global errlist_errors, spi_modules, i2c_modules, sd_modules, fram_modules
    global sch_errors, sch_modules
    if err_module in sch_modules and error in sch_errors:
        return sch_errors[error]
    elif err_module in spi_modules and error in spi_errors:
        return spi_errors[error]
    elif err_module in i2c_modules and error in i2c_errors:
        return i2c_errors[error]
    elif err_module in fs_modules and error in fs_errors:
        return fs_errors[error]
    elif err_module in fram_modules and error in fram_errors:
        return fram_errors[error]
    elif err_module in sd_modules and error in sd_errors:
        return sd_errors[error]
    elif err_module in bus_modules and error in bus_errors:
        return bus_errors[error]
    elif err_module in cam_modules and error in cam_errors:
        return cam_errors[error]
    elif err_module in proc_modules and error in proc_errors:
        return proc_errors[error]
    elif err_module == bldr_module:
        if error in bldr_errors:
            return bldr_errors[error]
    elif error in errlist_errors and error in errlist_errors:
        return errlist_errors[error]
    return hex(error)
