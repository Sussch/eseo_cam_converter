# A class to convert between J2000 and datetime.
#
# Indrek Synter 2015
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

from datetime import datetime, timedelta
import struct
import time


class J2000:
    """
    A class for converting J2000 to UTC datetime.
    http://stackoverflow.com/questions/19635860/convert-a-coordinate-of-j2000-to-current-date
    Based on Astronomical Algorithms Chapter 7.
    """

    def __init__(self, days, ms):
        self.days_past_j2000 = int(days)
        self.ms_of_day = int(ms)

    def to_datetime(self):
        """Convert the timestamp into a datetime structure."""
        jd = self.days_past_j2000 + 2451545.0
        # based on AA Ch7
        z = int(jd + 0.5)
        f = jd + 0.5 - z
        alpha = int((z - 1867216.25) / 36524.25)
        a = z + 1 + alpha - int(alpha / 4)
        b = a + 1524
        c = int((b - 122.1) / 365.25)
        d = int(365.25 * c)
        e = int((b - d) / 30.6001)
        day = b - d - int(30.6001 * e) + f
        d = int(day)  # day
        d_frac = day - d  # fractional day
        m = e - 1 if e < 14 else e - 13  # month
        y = c - 4716 if m > 2 else c - 4715  # year
        return datetime(y, m, d) + timedelta(days=d_frac, milliseconds=self.ms_of_day)

    @staticmethod
    def now():
        """Calculate the current timestamp."""
        # Based on:
        #  http://www.giss.nasa.gov/tools/mars24/help/algorithm.html
        #  http://jtauber.github.io/mars-clock/
        jdut = 2440587.5 + time.time() / 8.64e4
        jdtt = jdut + (35 + 32.184) / 8.64e4
        days_past_j2000 = jdtt - 2451545.0
        ms_of_day = int((days_past_j2000 % 1) * 8.64e7)
        days_past_j2000 = int(days_past_j2000)

        return J2000(days_past_j2000, ms_of_day)

    def to_binary(self):
        """Convert the timestamp into binary (8 B, little endian)."""
        return bytearray(struct.pack("<II", self.days_past_j2000, self.ms_of_day))

    def __repr__(self):
        """Convert the timestamp into a string for easier debugging."""
        return "(days_past_j2000 = {}, ms_of_day = {})".format(self.days_past_j2000, self.ms_of_day)

    @staticmethod
    def from_binary(binary):
        """Reproduce a timestamp from its binary form."""
        days_past_j2000, ms_of_day, = struct.unpack("<II", binary[:8])
        return J2000(days_past_j2000, ms_of_day)
