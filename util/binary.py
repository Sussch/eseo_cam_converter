# Utilities to convert or display binary.
#
# Henri Kuuste 2015
# Indrek Synter 2015, 2016
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import struct
from ast import literal_eval

PYTHON_TYPE_MAP = {
    'uint8': ('B', 1),
    'int8': ('b', 1),
    'uint16': ('H', 2),
    'int16': ('h', 2),
    'uint32': ('L', 4),
    'int32': ('l', 4),
    'uint64': ('Q', 8),
    'int64': ('q', 8),
    'float': ('f', 4),
    'double': ('d', 8),
    'string': ('s', -1),
    'bytearray': ('s', -1),
    'pad': ('x', 1)
}


def typecast_loose(value):
    """Performs loose type casting, based on variable value."""
    if value in [None, '', []]:
        return value
    return literal_eval(value)


class AttrDict(dict):
    """A class that maps a dictionary into its members"""
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self


def load_packet_data(packet, definitions):
    """Convert binary data into a dictionary, based on the structure declaration"""
    data = bytearray(packet)
    format_string = ''
    keys = []
    for d in definitions:
        if d['format_string'] == 's':
            size = struct.calcsize('<{}'.format(format_string))
            format_string += str(len(data) - size)
        format_string += d['format_string']
        keys.append(d['key'])
    values = struct.unpack('<{}'.format(format_string), bytearray(data))
    return AttrDict(zip(keys, values))


def to_hex_str(data):
    """Converts data to hex string."""
    data = bytearray(data)
    return ' '.join("{:02X}".format(x) for x in data)

