# Decompression of bit-packed pixel data.
#
# Henri Kuuste 2015
# Indrek Synter 2017, 2018
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import numpy
import math


def image_fast_unpack_12bit(data):
    """Unpacks unsigned 12-bit data to 16-bit numpy array"""
    length = len(data)
    # Allocate for the result
    output = numpy.zeros(int(math.ceil(8 * length / 12.0)), dtype=numpy.uint16)

    didx = 0
    bidx = 0
    while bidx < length:
        output[didx] = (data[bidx] << 4) | (data[bidx + 1] >> 4)
        output[didx + 1] = ((data[bidx + 1] & 0x0F) << 8) | data[bidx + 2]
        bidx += 3
        didx += 2

    return output


def image_fast_unpack_10bit(data):
    """Unpacks unsigned 10-bit data to 16-bit numpy array"""
    length = len(data)
    output = numpy.zeros(int(math.ceil(length * 8.0 / 10.0)), dtype=numpy.uint16)

    isrc = 0
    idest = 0

    while isrc < length:
        output[idest] = data[isrc] << 2

        if isrc + 1 < length:
            output[idest] |= data[isrc + 1] >> 6
            output[idest + 1] = (data[isrc + 1] & 0x3F) << 4

            if isrc + 2 < length:
                output[idest + 1] |= data[isrc + 2] >> 4
                output[idest + 2] = (data[isrc + 2] & 0x0F) << 6

                if isrc + 3 < length:
                    output[idest + 2] |= data[isrc + 3] >> 2
                    output[idest + 3] = (data[isrc + 3] & 0x03) << 8

                    if isrc + 4 < length:
                        output[idest + 3] |= data[isrc + 4]

        isrc += 5
        idest += 4

    return output


def pack_bits(data, src_bits, dst_bits, offset=0, unpacking=None):
    """Generic bit-packing or bit-unpacking"""
    output = []
    bidx = -1
    available = 0
    didx = -1
    remaining = 0
    mask = 0xFFFF
    mask = mask >> (16 - dst_bits)
    while didx < len(data):
        if remaining < 1:
            didx += 1
            remaining = src_bits
            if didx == 0:
                remaining -= offset
        if available < 1:
            bidx += 1
            output.append(0)
            available = dst_bits
        used = min(available, remaining)
        output[bidx] |= ((data[didx] >> max(remaining - available, 0)) << (available - used)) & mask
        remaining -= used
        available -= used
        if remaining < 1:
            didx += 1
            remaining = src_bits

    if unpacking:  # unpacking, discard partial data
        output = output[0: (len(data) * src_bits - offset) / dst_bits]

    return output
