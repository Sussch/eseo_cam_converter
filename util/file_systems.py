# Conversion functions for ESEO camera file systems.
#
# Indrek Synter 2015
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

from datetime import datetime


def convert_fatfs_date(fdate, ftime):
    """Converts FatFS date and time fields to Python datetime class."""
    try:
        year = ((fdate & 0xFE00) >> 9) + 1980
        month = ((fdate & 0x01E0) >> 5)
        day = fdate & 0x001F
        hour = (ftime & 0xF800) >> 11
        minute = (ftime & 0x07E0) >> 5
        second = (ftime & 0x001F) * 2

        return datetime(year, month, day, hour, minute, second)
    except:
        return datetime(1980, 1, 1, 0, 0, 0)


class FTPath:
    """Representation of the on-board numerical filepath format."""

    DEVICE_MASK = 0xF0000000
    DEVICE_OFFSET = 28
    ROOTDIR_MASK = 0x0F000000
    ROOTDIR_OFFSET = 24
    SUBDIR_MASK = 0x00FF0000
    SUBDIR_OFFSET = 16
    FILEID_MASK = 0x0000FFF0
    FILEID_OFFSET = 4
    EXTID_MASK = 0x0000000F
    EXTID_OFFSET = 0

    def __init__(self, path=None):
        if path:
            self.path = path
        else:
            self.path = 0x00000000

    def get_device(self):
        """Get device index."""
        return (self.path & FTPath.DEVICE_MASK) >> FTPath.DEVICE_OFFSET

    def get_root_dir(self):
        """Get root directory index."""
        return (self.path & FTPath.ROOTDIR_MASK) >> FTPath.ROOTDIR_OFFSET

    def get_subdir(self):
        """Get subdirectory index."""
        return (self.path & FTPath.SUBDIR_MASK) >> FTPath.SUBDIR_OFFSET

    def get_fileid(self):
        """Get file index."""
        return (self.path & FTPath.FILEID_MASK) >> FTPath.FILEID_OFFSET

    def get_extid(self):
        """Get extension index."""
        return self.path & FTPath.EXTID_MASK

    def set_fileid(self, fid):
        """Set file index."""
        self.path = self.path & (~FTPath.FILEID_MASK) | (fid << FTPath.FILEID_OFFSET)

    def set_path(self, path):
        """Set the whole filepath."""
        self.path = path

    def to_str(self):
        """Convert a numerical path to a string representation."""
        dev = self.get_device()
        rdir = self.get_root_dir()
        sdir = self.get_subdir()
        fid = self.get_fileid()
        extid = self.get_extid()

        result = "{:X}:".format(dev)

        if rdir > 0:
            result += "/{:X}".format(rdir)
        if sdir > 0:
            result += "/{:02X}".format(sdir)
        if fid > 0:
            result += "/{:03X}".format(fid)
        if extid > 0:
            result += ".{:X}".format(extid)
        return result
