# XML parser for ESEO camera command response structures.
#
# Indrek Synter 2015, 2018
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT


import xml.etree.ElementTree as ET

from string import Formatter
import re
import logging
import struct
import html

from util.binary import PYTHON_TYPE_MAP, typecast_loose, to_hex_str


class CTReplyMacro:
    def __init__(self):
        self.name = ""
        self.value = ""

    def parse_xml(self, branch):
        """Loads the parameter parameters from an XML branch."""
        self.name = branch.attrib["name"]
        self.value = branch.text


class CTReplyMacros:
    def __init__(self):
        self.macros = {}

    def add(self, macro):
        """Register a macro"""
        name = "#:{}".format(macro.name)
        self.macros[name] = macro

    def replace(self, text):
        output = text
        ml = set(re.findall(r'(\#:\w+)', text))
        for m in ml:
            # Replace #:<macro_name> with macro value
            if m in self.macros:
                output = output.replace(m, self.macros[m].value)
        return output


class CTReplyParameter:
    VERDICT_OK = '-'
    VERDICT_ERROR = '!'

    def __init__(self, macros=None):
        self.log = logging.getLogger("CC.Replies.Reply.Param")

        self.name = ""
        self.description = ""
        self.type = ""
        self.little_endian = True
        self.value = None

        # Formula for converting raw parameters
        self.formula = ""
        # Low and high limits for the converted value
        self.low_limit = None
        self.high_limit = None
        # Verdict, which indicates of conversion and limit checking result
        self.verdict = self.VERDICT_OK

        self.global_scope = {}
        self.local_scope = {}

        self.macros = macros

    def get_length(self):
        """Gets parameter length in bytes.
        Length is -1 for variable-length fields (string, bytearray)."""
        length = 0

        if self.type not in PYTHON_TYPE_MAP:
            self.log.error("Unknown parameter type \"{}\"".format(self.type))
        else:
            length = PYTHON_TYPE_MAP[self.type][1]

        return length

    def get_struct_format_string(self, length):
        """Gets the format string for extracting the parameter.
        The length field is used for variable-length parameters (string, bytearray)."""
        if self.little_endian:
            fmt_str = "<"
        else:
            fmt_str = ">"

        if self.type not in PYTHON_TYPE_MAP:
            self.log.error("Unknown parameter type \"{}\"".format(self.type))
        else:
            # Must specify length for string / bytearray types
            if self.type in ['string', 'bytearray']:
                fmt_str += str(length)

            fmt_str += PYTHON_TYPE_MAP[self.type][0]

        return fmt_str

    def from_binary(self, data):
        """Extracts parameter value from binary data
        and returns the leftover bytes."""
        length = self.get_length()

        # Take the segment that we need
        # and remember what was left over
        if length > 0:
            segment = data[0:length]
            leftover = data[length:]
        else:
            segment = data
            leftover = bytearray()
            length = len(data)

        self.value, = struct.unpack(self.get_struct_format_string(length), segment)

        if self.type == 'bytearray':
            self.value = bytearray(self.value)
        elif self.type == 'string':
            self.value = self.value.rstrip('\x00')

        try:
            # Convert values with a formula
            if len(self.formula) > 0:
                self.local_scope['x'] = self.value
                self.global_scope['mc'] = self.formula_check_mask
                self.global_scope['lc'] = self.formula_check_lim
                self.global_scope['lnz'] = self.formula_list_nonzero
                self.global_scope['ff'] = self.formula_field
                self.value = eval(self.formula, self.global_scope, self.local_scope)

            # Perform limit checking
            self.verdict = self.VERDICT_OK
            if self.low_limit and self.value < self.low_limit:
                self.verdict = self.VERDICT_ERROR
            if self.high_limit and self.value > self.high_limit:
                self.verdict = self.VERDICT_ERROR
        except Exception as e:
            self.log.exception(e)
            self.verdict = self.VERDICT_ERROR

        return leftover

    @staticmethod
    def formula_check_mask(value, mask, str_true, str_false):
        if (value & mask) == mask:
            return str_true
        return str_false

    @staticmethod
    def formula_check_lim(value, vmin, vmax, str_true, str_false):
        if value in range(vmin, vmax):
            return str_true
        return str_false

    @staticmethod
    def formula_field(value, mask):
        bitshift_count = 0
        for i in range(0, 32):
            if (mask & 1) == 0:
                bitshift_count += 1
                mask = mask >> 1
        return (value & mask) >> bitshift_count

    @staticmethod
    def formula_list_nonzero(delim, *arg):
        formula_list = []
        for a in arg:
            if a != "" and a != 0:
                formula_list.append(a)
        return delim.join(formula_list)

    def parse_xml(self, branch):
        """Loads the parameter parameters from an XML branch."""
        child_name = branch.find("name")
        if child_name is not None:
            self.name = child_name.text

        child_desc = branch.find("description")
        if child_desc is not None:
            self.description = child_desc.text

        child_formula = branch.find("formula")
        if child_formula is not None:
            self.formula = child_formula.text

        child_low = branch.find("low_limit")
        if child_low is not None:
            self.low_limit = child_low.text

        child_high = branch.find("high_limit")
        if child_high is not None:
            self.high_limit = child_high.text

        # Expand macros
        self.description = self.macros.replace(self.description)
        self.formula = self.macros.replace(self.formula)

        type_elem = branch.find("type")
        self.type = type_elem.text
        if "little_endian" in type_elem.attrib and ["little_endian"] == "false":
            self.little_endian = False

        # Convert low and high limits to the correct types
        self.low_limit = typecast_loose(self.low_limit)
        self.high_limit = typecast_loose(self.high_limit)


class CTReply:
    def __init__(self, macros=None):
        self.log = logging.getLogger("CC.Replies.Reply")
        self.formatter = Formatter()

        self.name = ""
        self.description = ""
        self.format_string = ""
        self.id = 0
        self.output_color = ""
        self.mute_for_subsystem = False

        self.parameters = []
        self.subsystems = []
        self.macros = macros

    def params_from_binary(self, data):
        """Interpret parameters from binary data."""
        # A human-readable dictionary of parameters
        params_dict = {}
        # A machine-readable dictionary of parameters
        raw_params_dict = {}

        for param in self.parameters:
            data = param.from_binary(data)

            raw_params_dict[param.name] = param.value
            if param.type == 'bytearray':
                params_dict[param.name] = to_hex_str(param.value)
            else:
                params_dict[param.name] = param.value

            # Add an additional parameter with verdict content
            params_dict['_v_' + param.name] = param.verdict

        return raw_params_dict, params_dict, data

    def string_response_from_binary(self, data):
        """Generate a string response, based on binary data."""
        response_dict = {
            "id": -1,
            "name": "",
            "text": "Unknown error",
            "color": "red",
            "binary": "",
            "binary_suffix": bytearray(),
            "params": {}
        }
        try:
            # Extract parameter dictionary from the binary data.
            # Remove the processed data segment while we're at it.
            raw_params_dict, params_dict, suffix = self.params_from_binary(data)

            # Use the format string to produce an output string from parameter dictionary
            if not self.mute_for_subsystem:
                output_str = self.format_string.format(**params_dict)
            else:
                output_str = ""

            # Add response ID and name into the parameters dictionary
            response_dict = {
                "id": self.id,
                "name": self.name,
                "text": output_str,
                "color": self.output_color,
                "binary": data,
                "binary_suffix": bytearray(suffix),
                "params": raw_params_dict
            }
        except KeyError as e:
            print(params_dict)
            self.log.error(
                'Referenced invalid parameter name "{}". Please check replies configuration.'.format(e.args[0]))
            response_dict["text"] = 'Invalid parameter "{}"'.format(e.args[0])
        except Exception as e:
            self.log.exception(e)

        return response_dict

    def check_subsystem(self, subsystem):
        """Check if a subsystem supports this reply."""
        if subsystem in self.subsystems:
            return True
        return False

    def generate_help(self):
        """Generate HTML help for the reply."""

        # TODO:: Looks lousy. Make it look better
        txt = '\t<a name="{}" class="reply"><b>{}</b>(<i>{}</i>)</a>:<br />\n'.\
            format(self.name, self.name, ', '.join([p.name for p in self.parameters]))
        txt += '\t\tReply ID: 0x{:X}<br />\n'.format(self.id)
        txt += '\t\tColor: {}<br />\n'.format(self.output_color)
        fmt_str = html.escape(self.format_string).replace('\r\n', '<br />').replace('\n', '<br />')
        txt += '\t\tFormat string:<p style="margin:0px 5px 2px 10px;color:{}">{}</p>\n'.\
            format(self.output_color, fmt_str)
        description = html.escape(self.description).replace('\r\n', '<br />').replace('\n', '<br />')
        txt += '\t\tDescription:<p style="margin:0px 5px 2px 10px;">{}</p>\n'.format(description)
        txt += '\t\tParams:\n\t\t" \
                "<table cellspacing="5" style="margin-left:10px"><tr><td>Type</td><td>Name</td>" \
                "<td>Description</td></tr>\n'

        for p in self.parameters:
            description = html.escape(p.description).replace('\r\n', '<br />').replace('\n', '<br />')
            txt += '\t\t\t<tr><td>{}</td><td><i>{}</i></td><td><p style="margin:0px;">{}</p></td>\n'.\
                format(p.type, p.name, description)
            txt += '\t\t</table><br />\n' \
                   '\t\tSupported on subsystems: <p style="margin:0px 5px 2px 10px;">{}</p><br />\n'.\
                format(', '.join(self.subsystems))

        return txt

    def generate_help_item(self):
        """Generate a HTML help list item."""
        return '<b>{}</b>(<i>{}</i>)'.format(self.name, ', '.join([p.name for p in self.parameters]))

    def parse_xml(self, branch):
        """Loads the reply parameters from XML."""
        self.name = branch.find("name").text
        self.description = branch.find("description").text
        self.format_string = branch.find("format_string").text
        self.output_color = branch.find("output_color").text

        # Expand macros
        self.description = self.macros.replace(self.description)

        id_str = branch.find("id").text
        self.id = int(id_str, 0)

        # Load list of parameters
        child = branch.find("parameters")
        for param in child:
            if param.tag == "param":
                parameter = CTReplyParameter(self.macros)
                parameter.parse_xml(param)

                self.parameters.append(parameter)

        # Load list of subsystems
        child = branch.find("subsystems")
        for subsys in child:
            if subsys.tag == "subsys":
                self.subsystems.append(subsys.text)


class CTReplies:
    def __init__(self):
        self.log = logging.getLogger("CC.Replies")
        self.xml_tree = None
        self.replies = {}
        self.reply_ids = {}
        self.macros = CTReplyMacros()

    def text_from_binary(self, subsystem, data):
        """Generates response text from binary data.
        The subsystem field is used for verification."""
        try:
            reply_id, reply_len, = struct.unpack("BB", data[:2])

            if (subsystem, reply_id) not in self.replies:
                self.log.error("Unknown reply {}: 0x{:02X}".format(subsystem, reply_id))
                return None

            reply = self.replies[(subsystem, reply_id)]
            result = reply.string_response_from_binary(data[2:(2 + reply_len)])
            result["binary_suffix"] += data[(2 + reply_len):]
            return result
        except Exception as e:
            self.log.exception(e)
        return None

    def text_from_headless_binary(self, subsystem, reply_name, data):
        """Generates response text from binary data that does
        not contain a response header."""
        try:
            if (subsystem, reply_name) not in self.reply_ids:
                self.log.error('Unknown reply {}: "{}"'.format(subsystem, reply_name))
                return None

            reply_id = self.reply_ids[(subsystem, reply_name)]
            reply = self.replies[(subsystem, reply_id)]
            result = reply.string_response_from_binary(subsystem, data)
            return result
        except Exception as e:
            self.log.exception(e)
        return None

    def generate_help(self):
        """Generate help for all replies."""
        # Generate a list of replies
        txt = '\t<h1>List of replies:</h1>\n'
        txt += '\t<p>This list is automatically generated from user-defined replies in ' \
               '<span class="file">config/replies.xml</span>.'
        txt += '\t\tReplies have been sorted by their binary identifiers.</p>\n'
        txt += '\t<ul class="replies">\n'

        for key in sorted(self.replies):
            txt += '\t\t<li class="reply"><a href="#{}">'.format(self.replies[key].name) + \
                   self.replies[key].generate_help_item() + '</a></li>\n'
        txt += '\t</ul>\n\t<div>\n'

        # Generate a sequence of reply descriptions
        for key in sorted(self.replies):
            txt += self.replies[key].generate_help()
        txt += '\t</div>\n'
        return txt

    def load_xml(self, path):
        """Load a configuration of replies from an XML file."""
        self.log.info("Loading replies from \"{}\".".format(path))

        try:
            self.xml_tree = ET.parse(path)
            root = self.xml_tree.getroot()

            for child in root:
                # Parse macros
                if child.tag == "macro":
                    replymacro = CTReplyMacro()
                    replymacro.parse_xml(child)

                    # Note that commands are mapped by name
                    self.macros.add(replymacro)
                # Parse replies
                elif child.tag == "reply":
                    reply = CTReply(self.macros)
                    reply.parse_xml(child)

                    # Map reply for all subsystems that support it
                    for subsys in reply.subsystems:
                        self.replies[(subsys, reply.id)] = reply
                        self.reply_ids[(subsys, reply.name)] = reply.id

        except Exception as e:
            self.log.exception(e)


ctg_replies = CTReplies()
