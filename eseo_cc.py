# File converter for extracting ESEO CAM images and logs from raw data.
#
# Indrek Synter 2018
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import argparse
import datetime
import logging
import sys
import os

from parsers.telemetry import ParserTelemetry
from util.replies import ctg_replies


class ESEO_CC:
    def __init__(self):
        self.log = logging.getLogger("CC")
        self.path_out = "output_" + self.get_str_date()

        self.format = "binary"

    @staticmethod
    def get_str_date():
        return datetime.datetime.now().strftime("%y%m%d_%H%M%S")

    def set_path_out(self, path):
        if path != "":
            self.path_out = path

    def set_input_format(self, format):
        self.format = format

    def parse(self, path, search=False):
        if not os.path.isdir(self.path_out):
            self.log.info("Directory {} does not exist yet, creating it.".format(self.path_out))
            os.makedirs(self.path_out)

        c = ParserTelemetry(self.path_out)

        if self.format == "binary":
            c.process_binary(path, search)
        elif self.format == "packets_hex_str":
            path_out_temp = os.path.join(self.path_out, "temp.bin")
            c.process_packets_hex_str(path, path_out_temp)
            c.process_binary(path_out_temp, search)
        elif self.format == "packets":
            path_out_temp = os.path.join(self.path_out, "temp.bin")
            c.process_packets(path, path_out_temp)
            c.process_binary(path_out_temp, search)


def init_logging(opts, logfile):
    # Configure logging
    if opts.verbose == 0:
        log_level = logging.WARNING
    elif opts.verbose == 1:
        log_level = logging.INFO
    elif opts.verbose == 2:
        log_level = logging.DEBUG
    else:
        log_level = logging.NOTSET

    logging.basicConfig(level=log_level, filename=logfile,
                        format='%(asctime)s: %(levelname)s: %(name)s: %(message)s')
    log = logging.getLogger("CC")

    # Note that basicConfig does not set up StreamHandler when supplied with
    # a filename. Add support for StreamHandler.
    #streamhandler = logging.StreamHandler()
    #streamhandler.setLevel(log_level)
    #log.addHandler(streamhandler)

    log.info('ESEO CAM Converter started..')

    return log


def main():
    parser = argparse.ArgumentParser(description="Extract ESEO camera images, logs from raw input data")
    parser.add_argument("input", type=str, help="Path to the input binary")
    parser.add_argument("-o", "--output", type=str, default="", help="Path for the output directory")
    parser.add_argument("-l", "--log", type=str, help="Path for the log file")
    parser.add_argument("-v", "--verbose", action="count", help="Increase log verbosity")
    parser.add_argument("-s", "--search", action="store_true", default=False, help="Search for potential file headers")
    parser.add_argument("-f", "--in-format", action="store", default="binary",
                        help="Input format, one of the following: binary, packets, packets_hex_str")

    args = parser.parse_args()

    log = init_logging(args, args.log)

    try:
        # Account for the path of the script.
        path_config_replies = os.path.join(os.path.dirname(sys.argv[0]), "config/replies.xml")
        ctg_replies.load_xml(path_config_replies)

        cc = ESEO_CC()
        cc.set_path_out(args.output)
        cc.set_input_format(args.in_format)
        cc.parse(args.input, args.search)
    except Exception as e:
        log.exception(e)


if __name__ == "__main__":
    main()
