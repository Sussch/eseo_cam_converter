# Parser for ESEO camera images.
#
# Indrek Synter 2016, 2018
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import os
import time
import json
import logging
from astropy.io import fits
import numpy

from parsers.generic import ParserGeneric
from util.binary import load_packet_data, to_hex_str
import util.image_conv as ic
from util.j2000 import J2000


class ParserImage(ParserGeneric):
    # Image header size on the flight
    SIZE_HEADER_IMAGE = 256
    # Flight version of the image header
    VERSION_HEADER_IMAGE = 6

    # Declaration of the structure of image header
    DECL_HEADER_IMAGE = [
        {'key': 'version', 'format_string': 'I'},
        {'key': 'crc', 'format_string': 'I'},
        {'key': 'slot_path', 'format_string': 'I'},
        {'key': 'file_size', 'format_string': 'I'},

        {'key': 'depth', 'format_string': 'B'},
        {'key': 'data_format', 'format_string': 'B'},
        {'key': 'compression', 'format_string': 'B'},
        {'key': 'flags', 'format_string': 'B'},

        {'key': 'trig_ts_days', 'format_string': 'I'},  # Trigger timestamp: days since J2000
        {'key': 'trig_ts_ms', 'format_string': 'I'},  # Trigger timestamp: milliseconds of day
        {'key': 'ts_days', 'format_string': 'I'},  # Timestamp: days since J2000
        {'key': 'ts_ms', 'format_string': 'I'},  # Timestamp: milliseconds of day
        {'key': 'window_x', 'format_string': 'I'},
        {'key': 'window_y', 'format_string': 'I'},
        {'key': 'window_w', 'format_string': 'I'},
        {'key': 'window_h', 'format_string': 'I'},
        {'key': 'col_dark_start', 'format_string': 'I'},
        {'key': 'col_dark_end', 'format_string': 'I'},
        {'key': 'row_dark_start', 'format_string': 'I'},
        {'key': 'row_dark_end', 'format_string': 'I'},
        {'key': 'exposure', 'format_string': 'f'},
        {'key': 'gain_r', 'format_string': 'f'},
        {'key': 'gain_g1', 'format_string': 'f'},
        {'key': 'gain_g2', 'format_string': 'f'},
        {'key': 'gain_b', 'format_string': 'f'},
        {'key': 'sensor_temp', 'format_string': 'f'},
        {'key': 'mcu_temp', 'format_string': 'f'},

        {'key': 'attitude_x', 'format_string': 'f'},
        {'key': 'attitude_y', 'format_string': 'f'},
        {'key': 'attitude_z', 'format_string': 'f'},
        {'key': 'attitude_w', 'format_string': 'f'},
        {'key': 'omega_x', 'format_string': 'f'},
        {'key': 'omega_y', 'format_string': 'f'},
        {'key': 'omega_z', 'format_string': 'f'},
        {'key': 'orbit_px', 'format_string': 'f'},
        {'key': 'orbit_py', 'format_string': 'f'},
        {'key': 'orbit_pz', 'format_string': 'f'},
        {'key': 'orbit_vx', 'format_string': 'f'},
        {'key': 'orbit_vy', 'format_string': 'f'},
        {'key': 'orbit_vz', 'format_string': 'f'},
        {'key': 'acs_ts_days', 'format_string': 'I'},  # Attitude timestamp: days since J2000
        {'key': 'acs_ts_ms', 'format_string': 'I'},  # Attitude timestamp: milliseconds of day

        {'key': 'pipeline_flags', 'format_string': 'I'},

        {'key': 'suffix', 'format_string': '96s'},
    ]

    # Image compression algorithm indices
    COMPRESSION_RAW = 0
    COMPRESSION_BITPACKED = 1
    COMPRESSION_ZIP = 2
    COMPRESSION_JPEG = 3

    def __init__(self, path_out):
        super().__init__(path_out)

        self.log = logging.getLogger("CC.TM.Img")

    def process(self, path_in):
        try:
            self.log.info("Processing {}".format(path_in))

            with open(path_in, 'rb') as fi:
                # Parse file header
                fhdr = self.parse_file_header(fi)
                if not fhdr:
                    raise ValueError("Image file is missing a generic file header")

                # Verify image header version
                fver = fhdr["version"] & self.MASK_FILE_VERSION
                if fver != self.VERSION_HEADER_IMAGE:
                    raise ValueError("Unsupported version (0x{:X}) of image header, expected 0x{:X}".
                                     format(fver, self.VERSION_HEADER_IMAGE))

                # Seek back to the beginning of the file
                fi.seek(0, os.SEEK_SET)

                # Read image header (includes generic file header).
                fillers = ['\xFF' * self.SIZE_HEADER_IMAGE, '\x00' * self.SIZE_HEADER_IMAGE]
                content = fi.read(self.SIZE_HEADER_IMAGE)
                if not content or len(content) < self.SIZE_HEADER_IMAGE:
                    raise ValueError("Image header is missing {} B of data".
                                     format(self.SIZE_HEADER_IMAGE - len(content)))
                if content in fillers:
                    raise ValueError("Instead of an image header, there are just filler bytes (0xFF or 0x00)")

                # Parse the image header
                img_hdr = load_packet_data(content, self.DECL_HEADER_IMAGE)
                # Read pixel data
                byte_data = fi.read()

                # Convert timestamp from J2000 to a more widely used format
                dt_utc = J2000(img_hdr.ts_days, img_hdr.ts_ms).to_datetime()
                img_hdr.timestamp = time.mktime(dt_utc.timetuple())

                if 'compression' in img_hdr.keys():
                    # Convert bit-packed images
                    if img_hdr['compression'] in [self.COMPRESSION_RAW, self.COMPRESSION_BITPACKED]:
                        if img_hdr['compression'] == self.COMPRESSION_RAW:
                            self.log.info("Converting a RAW iamge")
                        elif img_hdr['compression'] == self.COMPRESSION_BITPACKED:
                            self.log.info("Decompressing a bit-packed image")

                        ''' Work-around for missing image data (partial download) '''
                        full_bits = img_hdr.window_w * img_hdr.window_h * img_hdr.data_format
                        # add a byte if full_bits isn't byte-aligned
                        if full_bits % 8 > 0:
                            full_bits = full_bits + 8
                        bytes_missing = full_bits / 8 - len(byte_data)
                        if bytes_missing > 0:
                            self.log.warning("Missing {} bytes of pixel data".format(bytes_missing))
                            byte_data = byte_data + '\x00' * bytes_missing

                        # Convert to 16 bit array.
                        self.log.info("Converting pixel data from {} bits to 16 bits".format(img_hdr.data_format))
                        if img_hdr.data_format == 16:
                            pixel_data = numpy.fromstring(str(byte_data[0:full_bits / 8]), dtype=numpy.uint16)
                        elif img_hdr.data_format == 12:
                            pixel_data = ic.image_fast_unpack_12bit(bytearray(byte_data))
                        elif img_hdr.data_format == 10:
                            pixel_data = ic.image_fast_unpack_10bit(bytearray(byte_data))
                        else:
                            pixel_data = numpy.array(
                                ic.pack_bits(bytearray(byte_data), 8, img_hdr.data_format, 0, True),
                                dtype=numpy.uint16)

                        # Reshape from 1D to 2D
                        self.log.info("Reshaping pixel data from 1D to 2D array")
                        pixel_data = pixel_data.reshape((img_hdr.window_h, img_hdr.window_w))

                        # Image width, height
                        img_hdr.width = img_hdr.window_w + img_hdr.col_dark_start + img_hdr.col_dark_end
                        img_hdr.height = img_hdr.window_h + img_hdr.row_dark_start + img_hdr.row_dark_end

                        # Save in FITS format
                        path_out_file = os.path.join(self.path_out, '{}.fit'.format(os.path.basename(path_in)))
                        self.save_fits(path_out_file, img_hdr, pixel_data)

                        # Store file metadata
                        self.save_json(path_out_file, img_hdr)

                    # Process zipped images
                    elif img_hdr['compression'] == self.COMPRESSION_ZIP:
                        self.log.error("The following lossless compression is currently not supported:"
                                       "Debayer + MSST + Zip")

                    # Process jpg files
                    elif img_hdr['compression'] == self.COMPRESSION_JPEG:
                        path_out_file = os.path.join(self.path_out, '{}.jpg'.format(os.path.basename(path_in)))
                        self.log.info("Saving thumbnail image {}".format(path_out_file))

                        # Store JPEG
                        with open(path_out_file, 'wb') as fo:
                            fo.write(byte_data)

                        # Store file metadata
                        self.save_json(path_out_file, img_hdr)
                    else:
                        raise ValueError('Unsupported image compression format 0x{:X}'.format(img_hdr["compression"]))

        except Exception as e:
            self.log.exception(e)
    
    def save_json(self, path_out_file, img_hdr):
        """Store metadata from the image header in a JSON file"""
        # Convert binary string into something more easily representable in json.
        modif_hdr = img_hdr
        modif_hdr["suffix"] = to_hex_str(img_hdr["suffix"])

        path_out_file = path_out_file + '.json'
        with open(path_out_file, 'wt') as fo:
            json.dump(modif_hdr, fo, indent=4, sort_keys=True)

    def save_fits(self, path_out_file, img_hdr, fdata):
        """Save a raw image in the FITS format."""
        try:
            self.log.info("Saving FITS {}".format(path_out_file))

            # Create an empty FITS file
            hdu = fits.PrimaryHDU(fdata)
            hdulist = fits.HDUList([hdu])

            # Even though the data is uint16, convert to int16 to avoid astropy defaulting to a BZERO of 32768.
            hdu.data = numpy.int16(hdu.data)

            # Store some of the metadata in the FITS header.
            hdu.header['TSTAMP'] = img_hdr.timestamp
            hdu.header['TEMP'] = img_hdr.sensor_temp
            hdu.header['MCUTEMP'] = img_hdr.mcu_temp
            hdu.header['EXPOSURE'] = 1.0 / img_hdr.exposure
            hdu.header['EXPFREQ'] = img_hdr.exposure
            hdu.header['RGAIN'] = img_hdr.gain_r
            hdu.header['GGAIN'] = img_hdr.gain_g1  # for backward compatibility
            hdu.header['G1GAIN'] = img_hdr.gain_g1
            hdu.header['G2GAIN'] = img_hdr.gain_g2
            hdu.header['BGAIN'] = img_hdr.gain_b
            hdu.header['DEPTH'] = img_hdr.depth
            hdu.header['WND_X'] = img_hdr.window_x
            hdu.header['WND_Y'] = img_hdr.window_y
            hdu.header['WND_W'] = img_hdr.window_w
            hdu.header['WND_H'] = img_hdr.window_h
            hdu.header['DC_S'] = img_hdr.col_dark_start
            hdu.header['DR_S'] = img_hdr.row_dark_start
            hdu.header['VER'] = img_hdr.version
            hdu.header['COMPR'] = img_hdr.compression

            # Write to FITS.
            hdulist.writeto(path_out_file, overwrite=True)
        except Exception as e:
            self.log.exception(e)
