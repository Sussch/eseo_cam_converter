# Parser for ESEO camera statistx files (all sorts of counters).
#
# Indrek Synter 2016, 2018
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import os
import logging
import struct

from parsers.generic import ParserGeneric
from util.binary import load_packet_data

statistx_decl = {}
# Generic stuff - errors, resets
statistx_decl_generic = [
    {'key': 'num_boots', 'format_string': 'I', 'desc': 'Number of reboots in total'},
    {'key': 'num_errors', 'format_string': 'I', 'desc': 'Number of errors in total'},

    {'key': 'num_poweron_resets', 'format_string': 'I', 'desc': 'Number of power-on resets'},
    {'key': 'num_brownout_resets', 'format_string': 'I', 'desc': 'Number of broun-out resets (too low supply voltage)'},
    {'key': 'num_wdog_resets', 'format_string': 'I', 'desc': 'Number of watchdog resets'},
    {'key': 'num_sw_resets', 'format_string': 'I', 'desc': 'Number of software resets (reboot command)'},
    {'key': 'num_hard_faults', 'format_string': 'I', 'desc': 'Number of hard fault resets (fatal errors)'},
    {'key': 'num_malloc_failures', 'format_string': 'I', 'desc': 'Number of malloc failures (FreeRTOS out of heap)'},
    {'key': 'num_stack_overflows', 'format_string': 'I',
     'desc': 'Number of stack overflow resets (FreeRTOS task out of stack)'},

    {'key': 'num_bldr_errors', 'format_string': 'H',
     'desc': 'Number of bootloader errors (CRC mismatch, error on SPI FRAM access)'},
    {'key': 'num_ctab_errors', 'format_string': 'H',
     'desc': 'Number of configuration table errors (error on SPI FRAM access, wrong table version)'},
    {'key': 'num_sch_errors', 'format_string': 'H', 'desc': 'Number of scheduler errors (failed command)'},
    {'key': 'num_dt_sch_errors', 'format_string': 'H',
     'desc': 'Number of date-time scheduler errors (missed or failed command)'},
    {'key': 'num_errlog_backup', 'format_string': 'H',
     'desc': 'Number of error log fallbacks (error on SPI FRAM access)'}
]

# Imaging statistics
statistx_decl_imaging = [
    {'key': 'num_sensor_init_errors', 'format_string': 'I',
     'desc': 'Number of image sensor initialization failures (I2C error or already initialized)'},
    {'key': 'num_sensor_cfg_errors', 'format_string': 'I',
     'desc': 'Number of image sensor configuration failures (I2C error, invalid configuration)'},
    {'key': 'num_image_triggers', 'format_string': 'I', 'desc': 'Total number of image triggers (before filtering)'},
    {'key': 'num_images_taken', 'format_string': 'I',
     'desc': 'Total number of images taken successfully (after filtering)'},
    {'key': 'num_failed_images', 'format_string': 'I', 'desc': 'Total number of failed images'}
]

# P-CAM statistics
statistx_decl[0x57004001] = statistx_decl_generic + [
    {'key': 'num_fs0_reformats', 'format_string': 'B', 'desc': 'Number of SRAM file system reformats'},
    {'key': 'num_fs1_reformats', 'format_string': 'B', 'desc': 'Number of I2C FRAM file system reformats'},
    {'key': 'num_fs0_errors', 'format_string': 'B', 'desc': 'Number of SRAM file system errors'},
    {'key': 'num_fs1_errors', 'format_string': 'B', 'desc': 'Number of I2C FRAM file system errors'},

    {'key': 'num_i2c_errors', 'format_string': 'I', 'desc': 'Number of generic I2C errors'},
    {'key': 'num_i2c2_errors', 'format_string': 'I',
     'desc': 'Number of I2C2 errors (image sensor, FRAM or temperature sensor)'},

    {'key': 'num_can_errors', 'format_string': 'I', 'desc': 'Number of CAN errors (generic)'},
    {'key': 'num_can0_errors', 'format_string': 'I', 'desc': 'Number of errors on CAN Main (physical)'},
    {'key': 'num_can1_errors', 'format_string': 'I', 'desc': 'Number of errors on CAN Redundant (physical)'},
    {'key': 'num_cf_errors', 'format_string': 'I', 'desc': 'Number of CanFestival errors'},

    {'key': 'num_rs485_errors', 'format_string': 'I', 'desc': 'Number of RS485 errors'},
    {'key': 'num_usb_errors', 'format_string': 'I', 'desc': 'Number of USB errors'},
] + statistx_decl_imaging

# S-CAM statistics
statistx_decl[0x57008001] = statistx_decl_generic + [
    {'key': 'num_fs0_reformats', 'format_string': 'B', 'desc': 'Number of file system reformats on SDRAM1'},
    {'key': 'num_fs1_reformats', 'format_string': 'B', 'desc': 'Number of file system reformats on SDRAM2'},
    {'key': 'num_fs2_reformats', 'format_string': 'B', 'desc': 'Number of file system reformats on SPI FRAM1'},
    {'key': 'num_fs3_reformats', 'format_string': 'B', 'desc': 'Number of file system reformats on SPI FRAM2'},
    {'key': 'num_fs4_reformats', 'format_string': 'B', 'desc': 'Number of file system reformats on SPI FRAM3'},
    {'key': 'num_fs5_reformats', 'format_string': 'B', 'desc': 'Number of file system reformats on SPI FRAM4'},
    {'key': 'num_fs6_reformats', 'format_string': 'B', 'desc': 'Number of file system reformats on SPI SD Card 1'},
    {'key': 'num_fs7_reformats', 'format_string': 'B', 'desc': 'Number of file system reformats on SPI SD Card 2'},
    {'key': 'num_fs8_reformats', 'format_string': 'B', 'desc': 'Number of file system reformats on SPI SD Card 3'},
    {'key': 'num_fs9_reformats', 'format_string': 'B', 'desc': 'Number of file system reformats on SPI SD Card 4'},
    {'key': 'num_fs_errors', 'format_string': 'B', 'desc': 'Number of uncategorized file system errors'},
    {'key': 'num_fs0_errors', 'format_string': 'B', 'desc': 'Number of file system errors on SDRAM1'},
    {'key': 'num_fs1_errors', 'format_string': 'B', 'desc': 'Number of file system errors on SDRAM2'},
    {'key': 'num_fs2_errors', 'format_string': 'B', 'desc': 'Number of file system errors on SPI FRAM1'},
    {'key': 'num_fs3_errors', 'format_string': 'B', 'desc': 'Number of file system errors on SPI FRAM2'},
    {'key': 'num_fs4_errors', 'format_string': 'B', 'desc': 'Number of file system errors on SPI FRAM3'},
    {'key': 'num_fs5_errors', 'format_string': 'B', 'desc': 'Number of file system errors on SPI FRAM4'},
    {'key': 'num_fs6_errors', 'format_string': 'B', 'desc': 'Number of file system errors on SPI SD Card 1'},
    {'key': 'num_fs7_errors', 'format_string': 'B', 'desc': 'Number of file system errors on SPI SD Card 2'},
    {'key': 'num_fs8_errors', 'format_string': 'B', 'desc': 'Number of file system errors on SPI SD Card 3'},
    {'key': 'num_fs9_errors', 'format_string': 'B', 'desc': 'Number of file system errors on SPI SD Card 4'},

    {'key': 'num_sd_errors', 'format_string': 'I', 'desc': 'Number of driver errors on SPI SD Cards (generic)'},
    {'key': 'num_sd1_errors', 'format_string': 'I', 'desc': 'Number of driver errors on SPI SD Card 1'},
    {'key': 'num_sd2_errors', 'format_string': 'I', 'desc': 'Number of driver errors on SPI SD Card 2'},
    {'key': 'num_sd3_errors', 'format_string': 'I', 'desc': 'Number of driver errors on SPI SD Card 3'},
    {'key': 'num_sd4_errors', 'format_string': 'I', 'desc': 'Number of driver errors on SPI SD Card 4'},

    {'key': 'num_spi_errors', 'format_string': 'I', 'desc': 'Number of errors on SPI (generic)'},
    {'key': 'num_spi4_errors', 'format_string': 'I', 'desc': 'Number of errors on SPI4'},
    {'key': 'num_spi3_errors', 'format_string': 'I', 'desc': 'Number of errors on SPI3'},
    {'key': 'num_spi6_errors', 'format_string': 'I', 'desc': 'Number of errors on SPI6'},

    {'key': 'num_i2c_errors', 'format_string': 'I', 'desc': 'Number of generic errors on I2C'},
    {'key': 'num_i2c1_errors', 'format_string': 'I', 'desc': 'Number of errors on I2C1'},
    {'key': 'num_i2c2_errors', 'format_string': 'I', 'desc': 'Number of errors on I2C2'},

    {'key': 'num_can_errors', 'format_string': 'I', 'desc': 'Number of CAN errors (generic)'},
    {'key': 'num_can1_errors', 'format_string': 'I', 'desc': 'Number of CAN1 errors (physical)'},
    {'key': 'num_can2_errors', 'format_string': 'I', 'desc': 'Number of CAN2 errors (physical)'},
    {'key': 'num_cf_errors', 'format_string': 'I', 'desc': 'Number of CanFestival errors'},

    {'key': 'num_rs485_errors', 'format_string': 'I', 'desc': 'Number of RS485 errors'},
    {'key': 'num_usb_errors', 'format_string': 'I', 'desc': 'Number of USB errors'},
] + statistx_decl_imaging


def statistx_calc_len(version):
    """Calculate the length of the statistx file structure, given the file version"""
    fmt_str = ''.join([entry['format_string'] for entry in statistx_decl[version]])
    return struct.calcsize('<' + fmt_str)


# Lengths of the statistx file structures for different versions of the file
statistx_len = {
    0x57004001: statistx_calc_len(0x57004001),
    0x57008001: statistx_calc_len(0x57008001)}


class ParserStatistx(ParserGeneric):

    def __init__(self, path_out):
        super().__init__(path_out)

        self.log = logging.getLogger("CC.TM.Statistx")

    def process(self, path_in):
        try:
            self.log.info("Processing {}".format(path_in))

            with open(path_in, 'rb') as fi:
                # Parse file header
                fhdr = self.parse_file_header(fi)
                if not fhdr:
                    raise ValueError("Statistx file is missing a generic file header")

                ver = fhdr["version"]

                if ver not in statistx_len:
                    raise ValueError("Statistx file is of unknown version 0x{:08X}".format(ver))

                # Read task state
                content = fi.read(statistx_len[ver])
                if not content:
                    raise ValueError("Statistx file is missing everything but the header")
                elif len(content) != statistx_len[ver]:
                    raise ValueError("Statistx file is {} B, not {} B as expected.".
                                     format(len(content), statistx_len[ver]))

                # Parse it
                content = load_packet_data(content, statistx_decl[ver])

                # Produce the output file
                path_out_file = os.path.join(self.path_out, '{}_statistx.csv'.format(os.path.basename(path_in)))
                with open(path_out_file, 'wt+') as fo:
                    lines = 'Field,Value,Description\n'
                    # For each parameter
                    for d in statistx_decl[ver]:
                        key = d['key']
                        # Generate a CSV line
                        lines += '"{}",{},"{}"\n'.format(key, content[key], d['desc'])
                    # Append to the CSV
                    fo.write(lines + '\n')
        except Exception as e:
            self.log.exception(e)
