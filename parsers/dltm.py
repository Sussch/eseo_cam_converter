# Parser for ESEO camera telemetry headers.
#
# Indrek Synter 2016, 2018
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import os
import logging
import struct
import time

from parsers.generic import ParserGeneric
from util.j2000 import J2000


class ParserDLTM(ParserGeneric):
    SIZE_HEADER_DLTM = 10

    def __init__(self, path_out):
        super().__init__(path_out)

        self.log = logging.getLogger("CC.TM.DLTM")

    def process(self, path_in):
        try:
            self.log.info("Processing {}".format(path_in))

            with open(path_in, 'rb') as fi:
                # Parse file header
                fhdr = self.parse_file_header(fi)
                if not fhdr:
                    raise ValueError("DLTM file is missing a generic file header")

                fillers = ['\xFF' * self.SIZE_HEADER_DLTM, '\x00' * self.SIZE_HEADER_DLTM]
                content = fi.read(self.SIZE_HEADER_DLTM)
                if not content or len(content) < self.SIZE_HEADER_DLTM:
                    raise ValueError("DLTM file is missing {} B of data".
                                     format(self.SIZE_HEADER_DLTM - len(content)))
                if content in fillers:
                    raise ValueError("Instead of DLTM data, there are just filler bytes (0xFF or 0x00)")

                days_past_j2000, ms_of_day, compression_level, = struct.unpack('<IIB', bytearray(content[:9]))

                # Convert timestamp from J2000 to a more widely used format
                dt_utc = J2000(days_past_j2000, ms_of_day).to_datetime()

                fhdr.update({'timestamp': time.mktime(dt_utc.timetuple()), 'compression_level': compression_level})

                # Produce the output file
                path_out_file = os.path.join(self.path_out, '{}_dltm.txt'.format(os.path.basename(path_in)))
                with open(path_out_file, 'wt+') as fo:
                    fo.write("Telemetry header\n"
                             " Version: 0x{:08X}\n"
                             " Filepath: 0x{:08X}\n"
                             " CRC: 0x{:08X}\n"
                             " Size: 0x{:08X}\n".
                             format(fhdr['version'], fhdr['filepath'], fhdr['crc'], fhdr['size']))

                    fo.write(" Additional fields:\n"
                             "  Timestamp: {}\n"
                             "  Compression level: {}\n".
                             format(fhdr['timestamp'], fhdr['compression_level']))
        except Exception as e:
            self.log.exception(e)
