# Parser for ESEO camera error logs.
#
# Indrek Synter 2015, 2016, 2018
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import os
import logging

from parsers.generic import ParserGeneric
import util.errors as errors
from util.binary import load_packet_data, AttrDict

# Declaration of the structure of error log header.
errlog_hdr_decl = [
    {'key': 'file_version', 'format_string': 'I'},
    {'key': 'file_crc', 'format_string': 'I'},
    {'key': 'file_path', 'format_string': 'I'},
    {'key': 'file_size', 'format_string': 'I'},
    {'key': 'log_version', 'format_string': 'I'},
    {'key': 'hdr_crc', 'format_string': 'I'},
    {'key': 'num_resets', 'format_string': 'I'},
    {'key': 'num_errors', 'format_string': 'I'},
    {'key': 'start', 'format_string': 'I'},
    {'key': 'end', 'format_string': 'I'}
]
file_hdr_len = 16
errlog_hdr_len = 40

# Declaration of the structure of an error log entry.
errlog_entry_decl = [
    {'key': 'first_dsj2000', 'format_string': 'I'},
    {'key': 'first_msod', 'format_string': 'I'},
    {'key': 'last_dsj2000', 'format_string': 'I'},
    {'key': 'last_msod', 'format_string': 'I'},
    {'key': 'num_thrown', 'format_string': 'H'},
    {'key': 'exception', 'format_string': 'H'},
    {'key': 'subentry0', 'format_string': 'H'},
    {'key': 'subentry1', 'format_string': 'H'},
    {'key': 'subentry2', 'format_string': 'H'},
    {'key': 'subentry3', 'format_string': 'H'}
]
errlog_entry_len = 28


class ParserErrLog(ParserGeneric):
    FILE_HEADER_SIZE = errlog_hdr_len
    FILE_HEADER_VERSION_MASK = 0xFF

    def __init__(self, path_out):
        super().__init__(path_out)

        self.log = logging.getLogger("CC.TM.Err")

        self.errors = []
        self.num_errors = 0
        self.errlog_hdr = AttrDict()

    def process(self, path_in):
        try:
            self.log.info("Processing {}".format(path_in))

            with open(path_in, 'rb') as fi:
                # Read header
                content = fi.read(errlog_hdr_len)
                self.errlog_hdr = load_packet_data(content, errlog_hdr_decl)

                if self.errlog_hdr.end > self.errlog_hdr.start:
                    self.num_errors = self.errlog_hdr.end - self.errlog_hdr.start
                else:
                    self.num_errors = int((self.errlog_hdr.file_size - errlog_hdr_len - file_hdr_len) /
                                          errlog_entry_len)

                for i in range(0, self.num_errors):
                    content = fi.read(errlog_entry_len)
                    if not content or content == '':
                        break
                    elif len(content) < errlog_entry_len:
                        self.log.error("Missing {} B for a full error log entry, at 0x{:X}.".
                                       format(errlog_entry_len - len(content), fi.tell()))
                        break
                    error_entry = load_packet_data(content, errlog_entry_decl)
                    self.errors.append(error_entry)

                if len(self.errors) != self.num_errors:
                    self.log.error("Error log header claimed {} entries, whereas we only found {}".
                                   format(self.num_errors, len(self.errors)))

                head, tail = os.path.splitext(os.path.basename(path_in))
                path_out_log = os.path.join(self.path_out, head + ".log")
                self.save_log(path_out_log)

        except Exception as e:
            self.log.exception(e)

    def save_log(self, path):
        """Save the error log"""
        try:
            self.log.info("Saving error log to {}".format(path))
            with open(path, 'wt+') as fo:
                fo.write("File header:\n")
                fo.write("\tPath:        0x{:08X}\n".format(self.errlog_hdr.file_path))
                fo.write("\tVersion:     0x{:08X}\n".format(self.errlog_hdr.file_version))
                fo.write("\tSize:        0x{:08X}\n".format(self.errlog_hdr.file_size))
                fo.write("\tCRC:         0x{:08X}\n".format(self.errlog_hdr.file_crc))
                fo.write("Error log header:\n")
                fo.write("\tHeader CRC:  0x{:08X}\n".format(self.errlog_hdr.hdr_crc))
                fo.write("\tNum resets:  {}\n".format(self.errlog_hdr.num_resets))
                fo.write("\tNum errors:  {}\n".format(self.errlog_hdr.num_errors))
                fo.write("\tStart index: {}\n".format(self.errlog_hdr.start))
                fo.write("\tEnd index:   {}\n".format(self.errlog_hdr.end))

                fo.write("\nErrors:\n")

                for i in range(0, len(self.errors)):
                    # Print out exception code
                    error_code = self.errors[i]["exception"]
                    error_m = (error_code & 0xFF00) >> 8
                    error_ex = error_code & 0x00FF
                    fo.write("\t{}. {}: {}:\n".format(
                        i + self.errlog_hdr.start,
                        errors.format_module(error_m),
                        errors.format_error(error_ex, error_m)))
                    fo.write("\t\tCode:            0x{:04X}\n".format(error_code))

                    # Print out timestamps
                    fo.write("\t\tFirst thrown:    0x{:08X} 0x{:08X}\n".format(
                        self.errors[i]["first_dsj2000"], self.errors[i]["first_msod"]))
                    fo.write("\t\tLast thrown:     0x{:08X} 0x{:08X}\n".format(
                        self.errors[i]["last_dsj2000"], self.errors[i]["last_msod"]))

                    # Number of times that the exception has been thrown
                    num_thrown_raw = self.errors[i]["num_thrown"]
                    num_thrown = (num_thrown_raw & 0xFFF0) >> 4
                    num_subentries = num_thrown_raw & 0x000F
                    fo.write("\t\tNum thrown:      {} (0x{:04X})\n".format(num_thrown, num_thrown_raw))
                    # And sub-entries
                    if num_subentries > 4:
                        fo.write("\t\tNum sub-entries: {} (invalid, limited to 4)\n".format(num_subentries))
                        self.log.warning("Invalid number of sub-entries: {} (maximum is 4).".format(num_subentries))
                        num_subentries = 4
                    else:
                        fo.write("\t\tNum sub-entries: {}\n".format(num_subentries))

                    for j in range(0, num_subentries):
                        error_code = self.errors[i]["subentry" + str(j)]
                        error_m = (error_code & 0xFF00) >> 8
                        error_ex = error_code & 0x00FF
                        fo.write("\t\t\t{}: {}: {}: 0x{:04X}\n".format(
                            j,
                            errors.format_module(error_m),
                            errors.format_error(error_ex, error_m),
                            error_code))
        except Exception as e:
            self.log.exception(e)
