# Parser for ESEO camera telemetry files.
#
# Indrek Synter 2016, 2018
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import logging
import struct
import os
import re

from parsers.generic import ParserGeneric
from parsers.dltm import ParserDLTM
from parsers.image import ParserImage
from parsers.stdout import ParserStdout
from parsers.measurement_log import ParserLog
from parsers.proc import ParserProc
from parsers.statistx import ParserStatistx
from parsers.errorlog import ParserErrLog


class ParserTelemetry(ParserGeneric):
    SIZE_HSTX_PACKET = 892
    SIZE_HSTX_PACKET_HEADER = 12
    SIZE_HSTX_PACKET_CONTENT = SIZE_HSTX_PACKET - SIZE_HSTX_PACKET_HEADER
    HSTX_PACKET_MODE_IDLE = 0x07FF
    HSTX_PACKET_MODE_DATA = 0x0401

    FILE_HEADER_SIZE = 25

    FILE_PARSERS = {
        0x11000000: [ParserImage, "Image", '4'],
        0x57000000: [ParserStatistx, "Statistx", 'bj'],
        0x5F000000: [ParserStdout, "Stdout", 'bj'],
        0x6F000000: [ParserLog, "Log", 'bj'],
        0x7D000000: [ParserDLTM, "DLTM", 'bj'],
        0x7F000000: [ParserProc, "Proc", 'bj'],
        0xE1000000: [ParserErrLog, "ErrorLog", 'bj']}

    # Specific for ESEO camera software version on the flight.
    # These versions are listed only to make it easier to grasp the version regex.
    FILE_VERSIONS = [
        0x114A0006, 0x118A0006,
        0x57004001, 0x57008001,
        0x5F4A0001, 0x5F8A0001,
        0x6F4A0001, 0x6F8A0001,
        0x7D4A0001, 0x7D8A0001,
        0x7F4A0001, 0x7F8A0001,
        0xE10D0001, 0xE18A0001,
    ]

    # Specific for ESEO camera software version on the flight.
    REGEX_VERSION_FILE = b'[\x01\x06][\x40\x80\x00][\x4A\x4B\x8A\x8B\x00\x0D][\x11\x57\x5F\x6F\x7D\x7F\xE1]'

    def __init__(self, path_out):
        super().__init__(path_out)

        self.filecount = 0

        self.log = logging.getLogger("CC.TM")

    def find_first_file(self, fi):
        pos = fi.tell()
        content = fi.read()
        result = re.search(self.REGEX_VERSION_FILE, content)
        if result:
            if result.start() > 0:
                self.log.info("Found potential file at 0x{:X}".format(pos + result.start()))
            fi.seek(pos + result.start(), os.SEEK_SET)
            return True
        else:
            return False

    def process_binary(self, path_in, search=False):
        subfiles_to_process = []
        with open(path_in, "rb") as fi:
            addr, size = self.get_len_remaining(fi)
            self.log.info("Processing telemetry at 0x{:08X}, {} B".format(addr, size))

            while True:
                if search:
                    if not self.find_first_file(fi):
                        break

                try:
                    # Parse file header
                    fhdr = self.parse_file_header(fi)
                    if not fhdr:
                        break
                    # Validate file size
                    if fhdr['size'] > size:
                        raise ValueError("Invalid file size (0x{:08X} / 0x{:08X}) in telemetry".
                                         format(fhdr['size'], size))
                    # Check file type
                    if fhdr["type"] not in self.FILE_PARSERS.keys():
                        raise ValueError("Invalid file type 0x{:08X} in file version 0x{:08X}".
                                         format(fhdr["type"], fhdr["version"]))
                    # TODO:: Validate file version
                    # TODO:: Validate filepath, as well

                    # Read file contents
                    content = fi.read(fhdr['size'])
                    # Validate the length of content
                    if not content or len(content) < fhdr['size']:
                        raise ValueError("Missing {} B of data (expected 0x{:08X}, got 0x{:08X})".
                                         format(fhdr['size'] - len(content), fhdr["size"], len(content)))

                    # Generate file path
                    fname_suffix = '{}_{}{}.{}'. \
                        format(os.path.basename(path_in),
                               self.FILE_PARSERS[fhdr["type"]][1],
                               self.filecount,
                               self.FILE_PARSERS[fhdr["type"]][2])
                    path_out_file = os.path.join(self.path_out, fname_suffix)

                    self.log.info("Extracting file {} from address 0x{:08X}, size {} B".
                                  format(path_out_file, addr, fhdr["size"]))
                    # Produce the output file
                    with open(path_out_file, 'wb+') as fo:
                        # Write header
                        fo.write(fhdr['binary'])
                        # Write contents
                        fo.write(content)

                    # Enlist the output file for processing
                    subfiles_to_process.append((path_out_file, fhdr["type"]))

                    self.filecount += 1
                    # Re-align to 4 B boundary.
                    if not search:
                        self.realign(fi)
                except Exception as e:
                    self.log.exception(e)

        try:
            # Process the files that we've extracted so far
            for fpath, ftype in subfiles_to_process:
                parser_class = self.FILE_PARSERS[ftype][0]
                if parser_class:
                    parser = parser_class(self.path_out)
                    parser.process(fpath)
        except Exception as e:
            self.log.exception(e)

    def parse_hstx_packet(self, content):
        d = {}
        packet_header = content[:self.SIZE_HSTX_PACKET_HEADER]
        d["content"] = content[self.SIZE_HSTX_PACKET_HEADER:]

        d["prefix"], d["vcid"], d["frame_cnt"], d["pck_cnt"], d["suffix"], \
        d["mode"], d["pck_cnt2"], d["pck_data_size"] \
            = struct.unpack('>BBBBHHHH', bytearray(packet_header))

        assert d["prefix"] == 0x0A
        assert d["vcid"] & 0b11110001 == 0b01010000
        assert d["suffix"] == 0x1800
        assert d["mode"] in [self.HSTX_PACKET_MODE_IDLE, self.HSTX_PACKET_MODE_DATA]
        assert d["pck_data_size"] & 0xFC00 == 0

        d["vcid"] = (d["vcid"] & 0b00001110) >> 1
        d["pck_segment"] = (d["pck_cnt2"] & 0xC000) >> 14
        d["pck_cnt2"] = d["pck_cnt2"] & 0x3FFFF
        d["pck_data_size"] = d["pck_data_size"] & 0x3FF

        texts = []
        for key, val in d.items():
            if key != "content":
                texts.append("{}: 0x{:X}".format(key, val))
        self.log.debug("HSTX Packet: " + ", ".join(texts))

        # Use "pck_cnt2" as the packet sequence number.
        d["seq"] = d["pck_cnt2"]

        return d

    def parse_hstx_packet_file(self, fi):
        content = fi.read(self.SIZE_HSTX_PACKET)

        return self.parse_hstx_packet(content)

    def parse_hstx_packet_hex_file(self, fi):
        # Read HSTX packet with a \r\n suffix from a hex string file.
        content = self.read_from_hex_file(fi, self.SIZE_HSTX_PACKET)
        linefeed = fi.read(1)

        return self.parse_hstx_packet(content)

    def process_packets_hex_str(self, path_in, path_out_temp):
        last_seq = None

        with open(path_in, "rt") as fi, open(path_out_temp, 'wb+') as fo:
            addr, size = self.get_len_remaining(fi)
            self.log.info("Processing telemetry at 0x{:08X}, {} B".format(addr, size))

            while True:
                try:
                    # Parse HSTX packet header
                    addr = fi.tell()

                    if size - addr < self.SIZE_HSTX_PACKET:
                        break

                    d = self.parse_hstx_packet_hex_file(fi)

                    if d["content"][:4] != b'\x00\x00\x00\x00':
                        self.log.info("Found data at 0x{:08X}".format(addr))
                        num_missing_packets = d["seq"] - (last_seq + 1)
                        if num_missing_packets > 0:
                            self.log.warning("Missing {} packets at 0x{:08X}".format(num_missing_packets, addr))

                            d["content"] = b'\x00' * (num_missing_packets * self.SIZE_HSTX_PACKET_CONTENT) + d[
                                "content"]

                        # Write contents
                        fo.write(d["content"])

                    last_seq = d["seq"]
                except Exception as e:
                    self.log.exception(e)

    def process_packets(self, path_in, path_out_temp):
        last_seq = None

        with open(path_in, "rb") as fi, open(path_out_temp, 'wb+') as fo:
            addr, size = self.get_len_remaining(fi)
            self.log.info("Processing telemetry at 0x{:08X}, {} B".format(addr, size))

            while True:
                try:
                    # Parse HSTX packet header
                    addr = fi.tell()

                    if size - addr < self.SIZE_HSTX_PACKET:
                        break

                    d = self.parse_hstx_packet_file(fi)

                    if d["content"][:4] != b'\x00\x00\x00\x00':
                        self.log.info("Found data at 0x{:08X}".format(addr))

                        if last_seq:
                            num_missing_packets = d["seq"] - (last_seq + 1)

                            if num_missing_packets > 0:
                                self.log.warning("Missing {} packets at 0x{:08X}".format(num_missing_packets, addr))

                                d["content"] = b'\x00' * (num_missing_packets * self.SIZE_HSTX_PACKET_CONTENT) + d[
                                    "content"]

                        # Write contents
                        fo.write(d["content"])

                    last_seq = d["seq"]
                except Exception as e:
                    self.log.exception(e)
