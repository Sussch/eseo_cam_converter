# Parser for ESEO camera RTOS task lists.
#
# Indrek Synter 2016, 2018
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import os
import logging
import struct

from parsers.generic import ParserGeneric
from util.binary import load_packet_data


class ParserProc(ParserGeneric):
    DECL_TASK = [
        {'key': 'name', 'format_string': '10s'},
        {'key': 'index', 'format_string': 'B'},
        {'key': 'state', 'format_string': 'B'},
        {'key': 'priority', 'format_string': 'B'},
        {'key': 'run_time_percentage', 'format_string': 'B'},
        {'key': 'stack_free', 'format_string': 'H'}
    ]
    LEN_TASK = 16

    def __init__(self, path_out):
        super().__init__(path_out)

        self.reply_dict = {}
        self.csv_initialized = False
        self.csv_file = None

        self.log = logging.getLogger("CC.TM.Proc")

    def process(self, path_in):
        try:
            self.log.info("Processing {}".format(path_in))

            with open(path_in, 'rb') as fi:
                # Parse file header
                fhdr = self.parse_file_header(fi)
                if not fhdr:
                    raise ValueError("Log file is missing a generic file header")

                # Read the number of tasks
                content = fi.read(1)
                num_tasks, = struct.unpack('<B', content)

                # For each task
                for i in range(0, num_tasks):
                    # Read task state
                    content = fi.read(self.LEN_TASK)
                    if not content:
                        break
                    # Parse it
                    content = load_packet_data(content, self.DECL_TASK)

                    # TODO:: Make it possible to have multiple proc files within a single telemetry package.
                    self.append_csv("proc", content)

        except Exception as e:
            self.log.exception(e)

    def get_csv_path(self, name):
        """Get the path to the CSV file"""
        return os.path.join(self.path_out, name + '.csv')

    def init_csv(self, name):
        """Initialize the CSV file"""
        fname = self.get_csv_path(name)
        # Store the CSV header
        with open(fname, 'wt') as f:
            line = ''
            for x in self.DECL_TASK:
                line += x['key'] + ','
            f.write(line[:-1] + '\n')

        self.csv_initialized = True

    def append_csv(self, name, content):
        """Add task info to the CSV file"""
        fname = self.get_csv_path(name)
        # Initialize CSV, in case it doesn't exist already
        try:
            with open(fname):
                pass
            # Delete the file if it already exists, but hasn't been initialized
            if not self.csv_initialized:
                self.log.info('Deleting an already existing CSV file "{}"'.format(fname))
                os.remove(fname)
                self.init_csv(name)
        except IOError:
            self.init_csv(name)

        with open(fname, 'a') as fo:
            line = ''
            # For each task parameter
            for d in self.DECL_TASK:
                key = d['key']

                # Generate a CSV line
                if key == 'name':
                    name = content[key].split(bytearray('\0', 'ascii'))[0]
                    line += '"' + name.decode("ascii") + '",'
                else:
                    line += str(content[key]) + ','
            # Append to the CSV
            fo.write(line[:-1] + '\n')
