# Parser for ESEO camera measurement logs.
#
# Indrek Synter 2016, 2018
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import os
import logging
import struct

from parsers.generic import ParserGeneric
from util.replies import ctg_replies


class ParserLog(ParserGeneric):
    # The size of a command header in bytes
    SIZE_HEADER_CMD = 2

    def __init__(self, path_out):
        super().__init__(path_out)

        self.source_id = "Unknown"
        self.version_file = 0

        self.reply_dict = {}
        self.csv_initialized = {}

        self.log = logging.getLogger("CC.Log")

    def process(self, path_in):
        try:
            self.log.info("Processing {}".format(path_in))

            with open(path_in, 'rb') as fi:
                # Parse file header
                fhdr = self.parse_file_header(fi)
                if not fhdr:
                    raise ValueError("Log file is missing a generic file header")

                # Extract camera type from file version
                if (fhdr['version'] & 0x00F00000) == 0x00800000:
                    self.source_id = "S-CAM"
                elif (fhdr['version'] & 0x00F00000) in [0x00000000, 0x00400000]:
                    self.source_id = "P-CAM"
                # Remember the stdout file version
                self.version_file = fhdr["version"] & self.MASK_FILE_VERSION

                while True:
                    # Read timestamp
                    timestamp = self.parse_timestamp(fi)
                    if not timestamp:
                        # Only log an error if it's not the end of file
                        if fi.tell() < fhdr["size"] + self.SIZE_HEADER_GENERIC_FILE:
                            self.log.error("Missing timestamp at 0x{:X}".format(fi.tell()))
                        break

                    # Read command header
                    content = fi.read(self.SIZE_HEADER_CMD)
                    if not content or content == '':
                        self.log.error("Missing command header at 0x{:X}".format(fi.tell()))
                        break
                    res_id, res_len = struct.unpack('<BB', bytearray(content))

                    # Read command arguments
                    content = fi.read(res_len)
                    if not content or len(content) < res_len:
                        self.log.error("Missing command arguments at 0x{:X}".format(fi.tell()))
                        break

                    # Parse the reply
                    self.parse_reply(timestamp, res_id, content)

        except Exception as e:
            self.log.exception(e)

    def parse_reply(self, timestamp, reply_id, arguments):
        """Parse a command reply and its arguments"""
        # Cache reply structures
        if reply_id not in self.reply_dict:
            # Search for that reply
            reply_struct = None

            if (self.source_id, reply_id) in ctg_replies.replies:
                reply_struct = ctg_replies.replies[(self.source_id, reply_id)]
            else:
                self.log.warning('Unknown reply, ID {}'.format(reply_id))
                return
            # Bookmark it
            self.reply_dict[reply_id] = reply_struct
        # Seek reply structure by subsystem endpoint index and reply ID
        reply_struct = self.reply_dict[reply_id]

        content = []

        if not reply_struct:
            self.log.error('Invalid reply {}'.format(reply_id))
            return
        reply_name = reply_struct.name

        # Have terminal parse it
        raw_params_dict, params_dict, suffix = reply_struct.params_from_binary(arguments)
        for key, value in params_dict.items():
            content.append((key, value))
        # Append to CSV
        self.append_csv(timestamp, reply_name, content)

    def get_csv_path(self, name):
        """Get a path to the CSV file for the specific type of replies"""
        return os.path.join(self.path_out, name + '.csv')

    def init_csv(self, name, content):
        """Initialize the CSV output file"""
        fname = self.get_csv_path(name)
        # Store the CSV header
        with open(fname, 'wt') as f:
            line = 'log_timestamp,'
            for entry in content:
                line += str(entry[0]) + ','
            f.write(line[:-1] + '\n')
        self.csv_initialized[fname] = True

    def append_csv(self, timestamp, name, content):
        """Append a response to the CSV file"""
        fname = self.get_csv_path(name)
        # Initialize CSV, in case it doesn't exist already
        try:
            with open(fname):
                pass
            # Delete the file if it already exists, but hasn't been initialized
            if fname not in self.csv_initialized:
                self.log.info('Deleting an already existing CSV file "{}"'.format(fname))
                os.remove(fname)
                self.init_csv(name, content)
        except IOError:
            self.init_csv(name, content)

        with open(fname, 'a') as f:
            line = str(timestamp) + ','
            for entry in content:
                line += str(entry[1]) + ','
            f.write(line[:-1] + '\n')
