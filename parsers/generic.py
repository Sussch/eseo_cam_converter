# Generic parser class for extracting ESEO camera files from raw data.
#
# Indrek Synter 2018
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import logging
import struct
import os
import time

from util.binary import to_hex_str
from util.j2000 import J2000


class ParserGeneric:
    SIZE_HEADER_GENERIC_FILE = 16

    # Mask for file type identifier in file version
    MASK_FILE_TYPE = 0xFF000000
    # Mask for the version of file header and file internal structure
    MASK_FILE_VERSION = 0x0000FFFF
    # Mask for camera (primary, secondary) index in file version
    MASK_CAMERA_TYPE = 0x00F00000
    # Mask for camera firmware slot index in file version
    MASK_SLOT_ID = 0x000F0000

    def __init__(self, path_out):
        self.path_out = path_out

        self.log = logging.getLogger("CC.TM.G")

    @staticmethod
    def file_header_to_str(fhdr):
        return "(type: 0x{:08X}, version: 0x{:08X}, filepath: 0x{:08X}, size: {} B)".\
            format(fhdr["type"], fhdr["version"], fhdr["filepath"], fhdr["size"])

    def parse_file_header_str(self, addr, content):
        """Parse the generic file header in the input string"""
        fillers = ['\xFF' * self.SIZE_HEADER_GENERIC_FILE, '\x00' * self.SIZE_HEADER_GENERIC_FILE]
        if not content or content in fillers or len(content) < self.SIZE_HEADER_GENERIC_FILE:
            self.log.info("End of File; not a file header at 0x{:08X}: {} [{} B]".
                          format(addr, to_hex_str(content), len(content)))
            return None

        version, crc, filepath, size, = struct.unpack('<IIII', bytearray(content))
        d = {'version': version, 'crc': crc, 'filepath': filepath, 'size': size, 'binary': content,
             "type": version & self.MASK_FILE_TYPE}

        self.log.info("Address {:08X}: Header {}".format(addr, self.file_header_to_str(d)))

        return d

    def parse_file_header(self, fi):
        """Parse the generic file header in the input file"""
        addr = fi.tell()
        content = fi.read(self.SIZE_HEADER_GENERIC_FILE)
        return self.parse_file_header_str(addr, content)

    def read_from_hex_file(self, fi, size):
        content = fi.read(size * 2)
        return bytes.fromhex(content)

    def realign(self, fi):
        """Realign to word boundary."""
        # Dummy-read padding (if any).
        old_pos = fi.tell()
        padding = old_pos % 4
        if padding != 0:
            _ = fi.read(4 - padding)
            new_pos = fi.tell()
            self.log.info("Realigning address 0x{:08X} to 0x{:08X} (padding {} B)".format(old_pos, new_pos, padding))

    @staticmethod
    def get_len_remaining(fi):
        """Get the number of bytes still to be read"""
        addr = fi.tell()
        fi.seek(0, os.SEEK_END)
        size = fi.tell()
        fi.seek(addr, os.SEEK_SET)
        return addr, size

    @staticmethod
    def parse_timestamp(fi):
        """Parse entry timestamp."""
        # Read timestamp
        content = fi.read(8)
        fillers = ['\xFF' * 8, '\x00' * 8]
        if not content or content == '' or content in fillers:
            return None

        days_past_j2000, ms_of_day, = struct.unpack('<II', bytearray(content))

        # Convert timestamp from J2000 to a more widely used format
        dt_utc = J2000(days_past_j2000, ms_of_day).to_datetime()
        return time.mktime(dt_utc.timetuple())
