# Parser for ESEO camera stdout files (lists of command replies).
#
# Indrek Synter 2016, 2018
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import os
import logging
import struct

from datetime import datetime

import html

from parsers.generic import ParserGeneric
from util.replies import ctg_replies
from util.binary import load_packet_data, to_hex_str
from util.file_systems import FTPath, convert_fatfs_date


class ParserStdout(ParserGeneric):
    """Parser for standard output binaries."""

    SIZE_HEADER_CMD = 2

    ID_REPLY_LS = 633

    # Directory listing response header
    DECL_HEADER_LS = [
        {'key': 'path', 'format_string': 'I'},
        {'key': 'offset', 'format_string': 'B'},
        {'key': 'num', 'format_string': 'B'},
        {'key': 'result', 'format_string': 'B'},
    ]
    LEN_HEADER_LS = 7

    # Directory listing entry
    DECL_ENTRY_LS = [
        {'key': 'path', 'format_string': 'I'},
        {'key': 'mdate', 'format_string': 'H'},
        {'key': 'mtime', 'format_string': 'H'},
        {'key': 'fsize', 'format_string': 'I'}
    ]
    LEN_ENTRY_LS = 12

    def __init__(self, path_out):
        super().__init__(path_out)
        self.path_in = ""
        self.version_file = 0
        self.out_html = True
        self.reply_dict = {}
        self.source_id = "Unknown"

        self.log = logging.getLogger("CC.TM.Stdout")

    def process(self, path_in):
        try:
            self.path_in = path_in
            self.log.info("Processing {}".format(path_in))

            self.init_log()

            with open(path_in, 'rb') as fi:
                # Parse file header
                fhdr = self.parse_file_header(fi)
                if not fhdr:
                    raise ValueError("Stdout file is missing a generic file header")

                # Extract camera type from file version
                if (fhdr['version'] & 0x00F00000) == 0x00800000:
                    self.source_id = "S-CAM"
                elif (fhdr['version'] & 0x00F00000) in [0x00000000, 0x00400000]:
                    self.source_id = "P-CAM"
                # Remember the stdout file version
                self.version_file = fhdr["version"] & self.MASK_FILE_VERSION

                # Read the rest of the file
                while True:
                    # Read timestamp
                    timestamp = self.parse_timestamp(fi)
                    if not timestamp:
                        # Only log an error if it's not the end of file
                        if fi.tell() < fhdr["size"] + self.SIZE_HEADER_GENERIC_FILE:
                            self.log.error("Missing timestamp at 0x{:X}".format(fi.tell()))
                        break

                    # Read command header
                    cmd_hdr = fi.read(self.SIZE_HEADER_CMD)
                    if not cmd_hdr or cmd_hdr == '':
                        self.log.error("Missing command header at 0x{:X}".format(fi.tell()))
                        break
                    res_id, res_len = struct.unpack('<BB', bytearray(cmd_hdr))

                    # Read command arguments
                    cmd_content = fi.read(res_len)
                    if not cmd_content or len(cmd_content) < res_len:
                        self.log.error("Missing command arguments at 0x{:X}".format(fi.tell()))
                        break

                    # Parse the reply
                    d = self.parse_reply(cmd_hdr + cmd_content)
                    if d is None:
                        self.log.error("Failed to parse reply 0x{:X} of length {} B".format(res_id, res_len))
                        break
                    text_time = datetime.fromtimestamp(timestamp).strftime('%H:%M:%S')
                    # Append to the log
                    if self.out_html:
                        # Raw HTML content?
                        if 'html' in d:
                            self.append_log_html(text_time, html_snippet=d['html'])
                        # Text to be converted into HTML?
                        else:
                            self.append_log_html(text_time, d['text'], d['color'])
                    else:
                        self.append_log_text(text_time, d['text'])

        except Exception as e:
            self.log.exception(e)
        finally:
            self.close_log()

    def parse_reply(self, content):
        """Convert command response binary into a dictionary with human-readable fields."""
        d = ctg_replies.text_from_binary(self.source_id, content)
        if not d or 'name' not in d:
            raise ValueError("Failed to parse the reply {}".format(to_hex_str(content)))
        # Have a dedicated converter for the HTML output of ls().
        if d['name'] == 'ls':
            if self.out_html:
                d['html'] = self.ls_to_html(d)
        return d

    def get_log_path(self):
        """Get the path to the output log file (HTML or plaintext)."""
        # Output directory + source filename + _stdout
        fname = os.path.basename(self.path_in)
        fpath = os.path.join(self.path_out, os.path.splitext(fname)[0] + "_stdout")
        if self.out_html:
            fpath += ".html"
        else:
            fpath += ".log"
        return fpath

    def init_log(self):
        """Initialize the log (remove the file if it already exists)."""
        fpath = self.get_log_path()
        if os.path.exists(fpath):
            os.remove(fpath)

    def close_log(self):
        """Close the active log (HTML or plaintext)."""
        if self.out_html:
            with open(self.get_log_path(), 'a+') as f:
                # Write HTML footer.
                f.write("\n </body>\n</html>")

    def append_log_text(self, ttime, text):
        """Append text to a text log."""
        with open(self.get_log_path(), 'a') as f:
            f.write(ttime + ': ' + text + '\n')

    def append_log_html(self, ttime, text=None, color=None, html_snippet=None):
        """Append colored text or a HTML snippet to an HTML log."""
        # HTML snippet?
        if html_snippet is not None:
            text = html_snippet
        # Or plaintxt?
        elif text is not None:
            # Got to escape the text first.
            text = html.escape(text)
            # And replace newlines with "br" tags.
            text = text.replace('\r\n', '<br />\n')
            text = text.replace('\n', '<br />\n')

        # Is there a color specified?
        if color is None:
            output_str = "<span style='color:gray'>{}: </span><span>{}</span>".format(ttime, text)
        else:
            output_str = "<span style='color:gray'>{}: </span><span style='color:{}'>{}</span>". \
                format(ttime, color, text)

        # Write to file.
        with open(self.get_log_path(), 'a+') as f:
            # Just in case, I guess?
            f.seek(0, os.SEEK_END)
            # Write HTML header.
            if f.tell() == 0:
                f.write("<!DOCTYPE html>\n"
                        "<html>\n"
                        " <head>\n"
                        "  <title>Stdout, version 0x{:08X}</title>\n"
                        " </head>\n"
                        " <body>\n".format(self.version_file))
            # Write the entry.
            f.write(output_str + '<br />\n')

    def ls_to_html(self, response):
        """Convert a file listing response to an HTML table."""
        try:
            params = response["params"]
            num = params["num"]
            entries_len = num * self.LEN_ENTRY_LS
            # Listing entries in raw binary form
            bin_listing = params["listing"][:entries_len]

            out_str = '<table cellspacing="5"><tr><th>ID</th><th>Path</th><th>Type</th><th>Size</th>' \
                      '<th>Modified</th></tr>\n'
            for i in range(0, num):
                # Unpack from binary
                start = i * self.LEN_ENTRY_LS
                end = start + self.LEN_ENTRY_LS
                entry = load_packet_data(bin_listing[start:end], self.DECL_ENTRY_LS)

                # Directory or file?
                etype = ''
                if (entry.path & 0x0000FFF0) == 0:
                    etype += 'D'
                    # Last modified date isn't used for directories, I think
                    mdate = ''
                else:
                    etype += 'F'
                    # Last modified date
                    mdate = convert_fatfs_date(entry.mdate, entry.mtime).strftime("%d/%m/%Y %H:%M:%S")

                # Size in mebibytes?
                if entry.fsize >= 1024 * 1024:
                    size_kib = "{:.2f} MiB".format(entry.fsize / (1024 * 1024))
                # Size in kibibytes?
                elif entry.fsize >= 1024:
                    size_kib = "{:.2f} KiB".format(entry.fsize / 1024)
                # Size in bytes?
                else:
                    size_kib = "{} B".format(entry.fsize)

                path = FTPath(entry.path)

                # Generate a table row in HTML
                out_str += '<tr><td>{}</td><td>{}</td><td>{}</td><td style="text-align: right;">{}</td>' \
                           '<td style="text-align: right;">{}</td></tr>\n'. \
                    format(i, path.to_str(), etype, size_kib, mdate)

            out_str += '</table>\n'
            return out_str
        except Exception as e:
            self.log.exception(e)
